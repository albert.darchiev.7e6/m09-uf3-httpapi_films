package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Pelicula(val id: String,
                val titol: String,
                val any: String,
                val genere: String,
                val director: String)

val peliculaList = mutableListOf(
    Pelicula("1", "The Shawshank Redemption", "1994", "Drama, Policiaca", "Frank Darabont"),
    Pelicula("2", "The Godfather", "1972", "Drama, Mafia, Crimen", "Francis Ford Coppola"),
    Pelicula("3", "The Dark Knight", "2008", "Accion, Suspense, Aventura","Christopher Nolan"),
    Pelicula("4", "1917", "2019", "Drama, Historia","Sam Mendes"))