package com.example.routes

import com.example.models.Pelicula
import com.example.models.peliculaList
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

//http://127.0.0.1:8080

fun Route.peliculaRoutes() {
    route("/pelicula") {
        get("all") {
            if (peliculaList.isNotEmpty()) { call.respond(peliculaList)
            } else {
                call.respondText("No customers found", status = HttpStatusCode.OK)
            }
        }
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText("Missing id", status = HttpStatusCode.BadRequest)
            val id = call.parameters["id"]
            for (peli in peliculaList) {
                if (peli.id == id) return@get call.respond(peli)
            }
            call.respondText( "No customer with id $id", status = HttpStatusCode.NotFound)
        }
        post("add") {
            val pelicula = call.receive<Pelicula>()
            peliculaList.add(pelicula)
            call.respondText("Pelicula added successfully", status = HttpStatusCode.Created)
        }
        delete("delete/{id}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in peliculaList) {
                if (peliculaList.removeIf { it.id == id }) {
                    call.respondText("Customer removed correctly", status = HttpStatusCode.Accepted)
                } else {
                    call.respondText("Not Found", status = HttpStatusCode.NotFound)
                }
            }
        }



    }



    }


